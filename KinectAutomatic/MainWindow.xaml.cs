﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Kinect;
using WMPLib;
using System.Windows.Forms;
using System.Management.Automation.Runspaces;
using System.IO;

namespace KinectAutomatic
{
    public partial class MainWindow : Window
    {
        private bool music_started = false;

        public MainWindow()
        {
            InitializeComponent();
        }

        KinectSensor _sensor;
        WindowsMediaPlayer player = new WindowsMediaPlayer();
        int last_middle = 0;
        int imag_count = 0;
        int depth_count = 0;
        int song = 0;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            angle_plu.Foreground = Brushes.DarkBlue;
            angle_min.Foreground = Brushes.DarkBlue;
            add_song.Foreground = Brushes.DarkBlue;

            volume.Minimum = 0;
            volume.Maximum = 100;
            volume.Value = 100;

            distance.Minimum = 100;
            distance.Maximum = 300;
            distance.Value = 250;
            
            //player.URL = @"C:\Users\Boris\Desktop\The Weeknd - False Alarm.mp3";

            if (KinectSensor.KinectSensors.Count > 0) {
                _sensor = KinectSensor.KinectSensors[0];

                if (_sensor.Status == KinectStatus.Connected){
                    _sensor.ColorStream.Enable();
                    _sensor.DepthStream.Enable();
                    _sensor.SkeletonStream.Enable();
                    _sensor.AllFramesReady += _sensor_AllFramesReady;
                    _sensor.Start();
                }
                    
            }
        }

        private byte[] GenerateDepth(DepthImageFrame frame) {
            short[] rawDethData = new short[frame.PixelDataLength];
            frame.CopyPixelDataTo(rawDethData);

            Byte[] pixels = new byte[frame.Height * frame.Width * 4];

            const int Blue = 0, Green = 1, Red = 2;
            int middle = 0;

            for (int i = 0, color = 0; i < rawDethData.Length && color < pixels.Length; i++, color += 4)
            {
                int depth = rawDethData[i] >> DepthImageFrame.PlayerIndexBitmaskWidth;
                depth /= 7;
                middle += depth;
                if (depth != last_middle)
                {
                    pixels[color + Blue] = (byte)depth;
                    pixels[color + Green] = (byte)depth;
                    pixels[color + Red] = (byte)depth;
                }
                else {
                    pixels[color + Blue] = 0;
                    pixels[color + Green] = 0;
                    pixels[color + Red] = 255;
                }

            }

            middle /= rawDethData.Length;

            if (player.playState == WMPPlayState.wmppsUndefined)
            {
                if(play_list.Items.Count > 0) player.URL = play_list.Items.GetItemAt(song).ToString();

                if (song < play_list.Items.Count) song++;
                else song = 0;
            }

            if (middle < distance.Value + 10)
            {
                if (!music_started)
                {
                    Shell("(Get-WmiObject -Namespace root/WMI -Class WmiMonitorBrightnessMethods).WmiSetBrightness(1,80)");
                    music_started = true;
                    
                    player.controls.play();

                }
            }
            else {
                if (music_started)
                {
                    Shell("(Get-WmiObject -Namespace root/WMI -Class WmiMonitorBrightnessMethods).WmiSetBrightness(1,0)");
                    player.controls.pause();
                    music_started = false;
                }
            }

            return pixels;
        }

        public void writeImage(int Width, int Height, byte[] pixels, int stride, String fileName)
        {
        
            BitmapSource bit = BitmapSource.Create(
                Width,
                Height,
                96, 96,
                PixelFormats.Bgr32,
                null,
                pixels,
                stride);

            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bit));
            try
            {
                var fileStream = new System.IO.FileStream(@"C:\xampp\htdocs\KinectWeb\" + fileName, FileMode.Create);
                encoder.Save(fileStream);
                fileStream.Close();
            }
            catch (IOException) { }
        }
        
        private void _sensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            //throw new NotImplementedException();
            using (ColorImageFrame colorFrame = e.OpenColorImageFrame()) {
                if (colorFrame == null)
                    return;

                byte[] pixels = new byte[colorFrame.PixelDataLength];
                colorFrame.CopyPixelDataTo(pixels);

                int stride = colorFrame.Width * 4;

                if (imag_count == 15)
                {
                    writeImage(colorFrame.Width, colorFrame.Height, pixels, stride, "camera.png");
                    imag_count = 0;
                }
                else imag_count++;
                

                camera.Source = BitmapSource.Create(
                    colorFrame.Width,
                    colorFrame.Height,
                    96, 96,
                    PixelFormats.Bgr32,
                    null,
                    pixels,
                    stride);
            }

            using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
            {
                if (depthFrame == null)
                    return;

                byte[] pixels = GenerateDepth(depthFrame);

                int stride = depthFrame.Width * 4;

                if (depth_count == 15)
                {
                    writeImage(depthFrame.Width, depthFrame.Height, pixels, stride, "depth.png");
                    depth_count = 0;
                }
                else depth_count++;

                depth.Source = BitmapSource.Create(
                    depthFrame.Width,
                    depthFrame.Height,
                    96, 96,
                    PixelFormats.Bgr32,
                    null,
                    pixels,
                    stride);
            }
        }

        void StopKinect(KinectSensor sensor)
        {
            if (sensor != null) {
                sensor.Stop();
                sensor.AudioSource.Stop();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            StopKinect(_sensor);
        }

        private void angle_min_Click(object sender, RoutedEventArgs e)
        {
            if (_sensor.ElevationAngle >= _sensor.MinElevationAngle-5)
                try { _sensor.ElevationAngle -= 5; }
                catch (Exception ex){ }
        }

        private void angle_plu_Click(object sender, RoutedEventArgs e)
        {
            if (_sensor.ElevationAngle <= _sensor.MaxElevationAngle+5)
                try { _sensor.ElevationAngle += 5; }
                catch (Exception ex) { }
        }

        private void volume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            player.settings.volume = (int)volume.Value;
        }

        private void add_song_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog opnFileDlg = new OpenFileDialog();
            opnFileDlg.Multiselect = false;
            opnFileDlg.Filter = "(mp3,wav,mp4,mov,wmv,mpg,avi,3gp,flv)|*.mp3;*.wav;*.mp4;*.3gp;*.avi;*.mov;*.flv;*.wmv;*.mpg|all files|*.*";
            if (opnFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                play_list.Items.Add(opnFileDlg.FileName);
            }
        }

        private void Shell(string scriptText)
        {
            Runspace runspace = RunspaceFactory.CreateRunspace();
            runspace.Open();
            Pipeline pipeline = runspace.CreatePipeline();
            pipeline.Commands.AddScript(scriptText);
            pipeline.Commands.Add("Out-String");
            pipeline.Invoke();
            runspace.Close();
        }

        private void distance_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            distance_lab.Content = (int)distance.Value;
        }
    }
}

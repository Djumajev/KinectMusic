<!DOCTYPE html>
<html>

	<head>
		<title>KinectWeb</title>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	</head>

	<body>
		<img id="img_camera">
		<img id="img_depth">
		<script>
			$(document).ready(function() {
				setInterval(function(){
					$("#img_camera").attr("src", "camera.png?"+new Date().getTime());
					$("#img_depth").attr("src", "depth.png?"+new Date().getTime());
				},100);
			});
		</script>
	</body>
</html>